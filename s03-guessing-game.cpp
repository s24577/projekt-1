#include <iostream>
#include <string>
#include <ctime>

int main()
{
	int guess, liczba;
	srand(time(0));
	liczba = rand() % 100 + 1;


	do
	{
		std::cout << "Zgadnij liczbe od 0 do 100: ";
		std::cin >> guess;


		if (guess > liczba)
			std::cout << "Za duża!\n\n";
		else if (guess < liczba)
			std::cout << "Za mała!\n\n";
		else
			std::cout << "\nZgadłeś! ";
	} while (guess != liczba);

	return 0;
}
