#include <iostream>

auto main(int argc,char*argv[]) -> int
{
int odstep;
int y=std::atoi(argv[1]);


    for(int i = 1, x = 0; i <= y; ++i, x = 0)
    {
        for(odstep= 1; odstep <= y-i; ++odstep)
        {
            std::cout <<" ";
        }

        while(x != 2*i-1)
        {
            std::cout << "*";
            ++x;
        }
        std::cout << std::endl;
    }
    return 0;
}
